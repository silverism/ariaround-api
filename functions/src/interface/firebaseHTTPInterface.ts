const cookieParser = require("cookie-parser")();
import * as express from "express";

import { 
  createUser,
  getShopList , 
  exchangeCoin , 
  getUser , 
  scanQRForSpend , 
  scanQRForEarn ,
  reward ,
  checkExistUser,
  checkReferral,
  updateUser,
  getShopItem,
  redeem,
  getDonateLocation,
  getTicketsByUser
} from "./../service/firebaseService"

import { getConstant } from "./../service/firebaseConstantService"


import { encryptData , decryptData } from "./../service/encryptService"


import { 
  moveAllDocument ,
  addShop ,
  addShopItem,
  generateRedeemCode,
  addDonateLocation
} from "./../service/firebaseAdminService"



const cors = require("cors")({ origin : true });

// const getCorsWithWhitelist = require('./../interface/cormanager')

const firebaseHTTPInterfaceApp = express();
firebaseHTTPInterfaceApp.use(cookieParser);
firebaseHTTPInterfaceApp.use(cors);

express.json()

module.exports = firebaseHTTPInterfaceApp;

firebaseHTTPInterfaceApp.get("/getShopList/:itemType", (req, res) => {

  
  getShopList(req.params.itemType).then(promiseResp => {
    console.log('return resolve' , promiseResp)
    res.send({ date: new Date(), status: "Complete", data: promiseResp })
  }).catch(error => {
    console.log('return error' , error)
    res.send({ date: new Date(), status: "Error", data: error })
  })
  
})

firebaseHTTPInterfaceApp.get("/getShopItem/:shopId", (req, res) => {

  getShopItem(req.params.shopId).then(promiseResp => {
    console.log('return resolve' , promiseResp)
    res.send({ date: new Date(), status: "Complete", data: promiseResp })
  }).catch(error => {
    console.log('return error' , error)
    res.send({ date: new Date(), status: "Error", data: error })
  })
})

firebaseHTTPInterfaceApp.get("/getUser/:uid", (req, res) => {

  getUser(req.params.uid).then(promiseResp => {
    // console.log('return resolve' , promiseResp)
    res.send({ date: new Date(), status: "Complete", data: promiseResp })
  }).catch(error => {
    console.log('return error' , error)
    res.send({ date: new Date(), status: "Error", data: error })
  })
  
})

firebaseHTTPInterfaceApp.get("/checkExistUser/:uid", (req, res) => {

  checkExistUser(req.params.uid).then(promiseResp => {
    // console.log('return resolve' , promiseResp)
    res.send({ date: new Date(), status: "Complete", data: promiseResp })
  }).catch(error => {
    console.log('return error' , error)
    res.send({ date: new Date(), status: "Error", data: error })
  })
  
})

firebaseHTTPInterfaceApp.post("/exchangeCoin/", (req, res) => {
  let data = req.body

  exchangeCoin(data).then(promiseResp => {
    res.send({ date: new Date(), status: "Complete", data: "" })
  }).catch(error => {
    res.send({ date: new Date(), status: "Error", data: error })
  })
})

firebaseHTTPInterfaceApp.post("/reward/", (req, res) => {
  let data = req.body

  reward(data).then(promiseResp => {
    res.send({ date: new Date(), status: "Complete", data: "" })
  }).catch(error => {
    res.send({ date: new Date(), status: "Error", data: error })
  })
})

firebaseHTTPInterfaceApp.post("/encryptQR/", (req, res) => {

  let qrString = req.body.qrString

  console.log('qrString' , qrString)

  let qrEncryptString = encryptData(qrString)

  console.log('qrEncryptString' , qrEncryptString)

  
  res.send({ date: new Date(), status: "Complete", data: qrEncryptString })
  
})

firebaseHTTPInterfaceApp.post("/decryptQR/", (req, res) => {

  let qrString = req.body.qrString

  console.log('qrString' , qrString)

  let qrDecryptString = decryptData(qrString)

  console.log('qrDecryptString' , qrDecryptString)

  
  res.send({ date: new Date(), status: "Complete", data: qrDecryptString })
  
})

firebaseHTTPInterfaceApp.get("/constant/", (req, res) => {
  getConstant().then(promiseResp => {
    console.log('return resolve' , promiseResp)
    res.send({ date: new Date(), status: "Complete", data: promiseResp })
  }).catch(error => {
    console.log('return error' , error)
    res.send({ date: new Date(), status: "Error", data: error })
  })
})

firebaseHTTPInterfaceApp.post("/scanQRForSpend/", (req, res) => {

  const data = req.body

  scanQRForSpend(data).then(promiseResp => {
    console.log('return resolve' , promiseResp)
    res.send({ date: new Date(), status: "Complete", data: promiseResp })
  }).catch(error => {
    console.log('return error' , error)
    res.send({ date: new Date(), status: "Error", data: error })
  })
})

firebaseHTTPInterfaceApp.post("/scanQRForEarn/", (req, res) => {

  const data = req.body

  scanQRForEarn(data).then(promiseResp => {
    console.log('return resolve' , promiseResp)
    res.send({ date: new Date(), status: "Complete", data: promiseResp })
  }).catch(error => {
    console.log('return error' , error)
    res.send({ date: new Date(), status: "Error", data: error })
  })
})

firebaseHTTPInterfaceApp.post("/createUser/", (req, res) => {

  createUser(req.body).then(promiseResp => {
    // console.log('return resolve' , promiseResp)
    res.send({ date: new Date(), status: "Complete", data: promiseResp })
  }).catch(error => {
    console.log('return error' , error)
    res.send({ date: new Date(), status: "Error", data: error })
  })
  
})

firebaseHTTPInterfaceApp.post("/updateUser/", (req, res) => {

  updateUser(req.body).then(promiseResp => {
    // console.log('return resolve' , promiseResp)
    res.send({ date: new Date(), status: "Complete", data: promiseResp })
  }).catch(error => {
    console.log('return error' , error)
    res.send({ date: new Date(), status: "Error", data: error })
  })
  
})

firebaseHTTPInterfaceApp.post("/checkReferral/", (req, res) => {
  checkReferral(req.body).then(promiseResp => {
    // console.log('return resolve' , promiseResp)
    res.send({ date: new Date(), status: "Complete", data: promiseResp })
  }).catch(error => {
    console.log('return error' , error)
    res.send({ date: new Date(), status: "Error", data: error })
  })
})

firebaseHTTPInterfaceApp.post("/redeem/", (req, res) => {
  redeem(req.body).then(promiseResp => {
    // console.log('return resolve' , promiseResp)
    res.send({ date: new Date(), status: "Complete", data: promiseResp })
  }).catch(error => {
    console.log('return error' , error)
    res.send({ date: new Date(), status: "Error", data: error })
  })
})

firebaseHTTPInterfaceApp.post("/generateRedeemCode/", (req, res) => {
  generateRedeemCode(req.body).then(promiseResp => {
    // console.log('return resolve' , promiseResp)
    res.send({ date: new Date(), status: "Complete", data: promiseResp })
  }).catch(error => {
    console.log('return error' , error)
    res.send({ date: new Date(), status: "Error", data: error })
  })
})

firebaseHTTPInterfaceApp.post("/addDonateLocation/", (req, res) => {
  addDonateLocation(req.body).then(promiseResp => {
    // console.log('return resolve' , promiseResp)
    res.send({ date: new Date(), status: "Complete", data: promiseResp })
  }).catch(error => {
    console.log('return error' , error)
    res.send({ date: new Date(), status: "Error", data: error })
  })
})

firebaseHTTPInterfaceApp.get("/getDonateLocation/", (req, res) => {
  getDonateLocation().then(promiseResp => {
    // console.log('return resolve' , promiseResp)
    res.send({ date: new Date(), status: "Complete", data: promiseResp })
  }).catch(error => {
    console.log('return error' , error)
    res.send({ date: new Date(), status: "Error", data: error })
  })
})

// Temp method
firebaseHTTPInterfaceApp.post("/moveAllDocument/", (req, res) => {
  const data = req.body

  moveAllDocument(data).then(promiseResp => {
    console.log('return resolve' , promiseResp)
    res.send({ date: new Date(), status: "Complete", data: "" })
  }).catch(error => {
    console.log('return error' , error)
    res.send({ date: new Date(), status: "Error", data: error })
  })
})

firebaseHTTPInterfaceApp.post("/addShop/", (req, res) => {
  const data = req.body

  addShop(data).then(promiseResp => {
    console.log('return resolve' , promiseResp)
    res.send({ date: new Date(), status: "Complete", data: promiseResp })
  }).catch(error => {
    console.log('return error' , error)
    res.send({ date: new Date(), status: "Error", data: error })
  })
})

firebaseHTTPInterfaceApp.post("/addShopItem/", (req, res) => {
  const data = req.body

  addShopItem(data).then(promiseResp => {
    console.log('return resolve' , promiseResp)
    res.send({ date: new Date(), status: "Complete", data: promiseResp })
  }).catch(error => {
    console.log('return error' , error)
    res.send({ date: new Date(), status: "Error", data: error })
  })
})

firebaseHTTPInterfaceApp.post("/getTicketsByUser/", (req, res) => {
  const data = req.body

  getTicketsByUser(data).then(promiseResp => {
    console.log('return resolve' , promiseResp)
    res.send({ date: new Date(), status: "Complete", data: promiseResp })
  }).catch(error => {
    console.log('return error' , error)
    res.send({ date: new Date(), status: "Error", data: error })
  })
})