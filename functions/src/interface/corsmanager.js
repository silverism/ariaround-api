exports.getCorsWithWhitelist = function() {
    const cors = require('cors');
    console.log('getCorsWithWhitelist')
    const whitelist = [
        'http://localhost:4200',
        'http://localhost:5001',
        'https://ari-around-2021.web.app'
    ];

    const corsOptionsDelegate = function(req, callback) {
        console.log('req', req)
        var corsOptions;
        if (whitelist.indexOf(req.header('Origin')) !== -1) {
            corsOptions = { origin: true } // reflect (enable) the requested origin in the CORS response
        } else {
            corsOptions = { origin: false } // disable CORS for this request
        }
        callback(null, corsOptions) // callback expects two parameters: error and options
    }
    return cors(corsOptionsDelegate);
}

exports.getCorsOriginTrue = function() {
    const cors = require('cors');
    return cors({ origin: true });
}