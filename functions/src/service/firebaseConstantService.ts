import * as admin from 'firebase-admin';

const db = admin.firestore();



export function getConstant() {

  let genderList: any = []

  let promise = new Promise((resolve, reject) => {
    db.collection('CONSTANT').get().then(resp => {

      console.log('call db done')

      resp.forEach(element => {
        genderList.push({ id: element.id, ...element.data() })
      });

      console.log('genderList', genderList)

      resolve(genderList)
    }).catch(error => {
      reject(error)
    })
  })

  return promise

}