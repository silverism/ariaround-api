import * as admin from 'firebase-admin';

import { decryptData } from './encryptService'

import { CONST_REFERAL , randomUid } from './constant'

const db = admin.firestore();

export function getShopList(data: any) {

  let itemType = data

  let earnList: any = []

  let promise = new Promise((resolve, reject) => {

    db.collection('SHOP').where('available' , '==' , true).get().then(resp => {

      console.log('call db done')

      let shopPromiseArray : any = []

      resp.forEach(element => {

        shopPromiseArray.push(new Promise((resolve , reject) => {
          let shop : any = { uid : element.id, ...element.data() }

          console.log('shop' , shop)

          new Promise((resolve , reject) => {

            if (itemType == 'all'){

              db.collection('SHOP').doc(shop.uid).collection('ITEM').get().then(itemResp => {
              
                let items : any = []
      
                itemResp.forEach(itemElement => {
                  items.push({ uid : itemElement.id, ...itemElement.data() })
                });

                resolve(items)
                
              })

            }else{

              db.collection('SHOP').doc(shop.uid).collection('ITEM').where('type' , '==' , itemType).get().then(itemResp => {
              
                let items : any = []
      
                itemResp.forEach(itemElement => {
                  items.push({ uid : itemElement.id, ...itemElement.data() })
                });

                resolve(items)
                
              })

            }

          }).then(promiseResp => {
            shop.item = promiseResp
            resolve(shop)
          })
        }))
      })

      Promise.all(shopPromiseArray).then(resp => {
        resp.forEach(element => {
          earnList.push(element)
        });

        console.log('earnList', earnList)

        resolve(earnList)
      })

      
    }).catch(error => {
      reject(error)
    })
  })

  return promise

}

export function getShopItem(data: any) {
  let shopId = data

  let itemList : any = []

  let promise = new Promise((resolve, reject) => {
    
    db.collection('SHOP').doc(shopId).collection('ITEM').where('available' , '==' , true).get().then(itemResp => {
      itemList = []
      itemResp.forEach(element => {
        itemList.push({ uid : element.id, ...element.data() })
      });
      resolve(itemList)
    }).catch(error => {
      reject(error)
    })

  })

  return promise
}

export function scanQRForSpend(data: any) {

  let qrData = data.qrString

  let qrDecryptData = decryptData(qrData)

  // let qrDecryptDataArray = qrDecryptData.split('#')
  // let uidTo = qrDecryptDataArray[0]
  // let type = qrDecryptDataArray[1]

  let resultData : any = {
    "destination_id" : qrDecryptData
  }

  let promise = new Promise((resolve, reject) => {

    db.collection('SHOP').doc(qrDecryptData).get().then(resp => {

      console.log('call db done')

      if (resp.data() !== undefined){

        var shop : any = {uid : resp.id ,... resp.data()}

        console.log('resp.data()', resp.data())

        db.collection('SHOP').doc(qrDecryptData).collection('ITEM').where('type' , '==' , "spend").where('available' , '==' , true).get().then(itemResp => {
          let items : any = []
          itemResp.forEach(element => {
            items.push({uid : element.id , ... element.data() })
          });

          shop.item = items

          resultData.destination = shop

          resolve(resultData)

        }).catch(error => {
          reject(error)
        })
      }else{
        reject("No shop")
      }
    }).catch(error => {
      reject(error)
    })

  })

  return promise
}

export function scanQRForEarn(data: any) {

  let qrData = data.qrString

  let qrDecryptData = decryptData(qrData)

  let qrDecryptDataArray = qrDecryptData.split('#')
  let shopId = qrDecryptDataArray[0]
  let itemId = qrDecryptDataArray[1]
  let amount = qrDecryptDataArray[2]

  let resultData : any = {
    "destination_id" : shopId
  }

  let promise = new Promise((resolve, reject) => {

    db.collection('SHOP').doc(shopId).get().then(resp => {

      console.log('call db done')

      console.log('resp', resp)

      var shop : any = {uid : resp.id ,... resp.data()}

      db.collection('SHOP').doc(shopId).collection('ITEM').doc(itemId).get().then(itemResp => {
        
        let items : any = {uid : itemResp.id , ... itemResp.data() }
        
        let value = parseFloat(items.value) * parseFloat(amount)

        resultData.destination = shop
        resultData.item = items
        resultData.value = value

        resolve(resultData)

      }).catch(error => {
        reject(error)
      })
    }).catch(error => {
      reject(error)
    })

  })

  return promise
}

export function scanQRForTransfer(data: any) {

  let qrData = data.qrData

  let qrDecryptData = decryptData(qrData)

  let qrDecryptDataArray = qrDecryptData.split('#')
  let uidTo = qrDecryptDataArray[0]
  let type = qrDecryptDataArray[1]

  let resultData : any = {
    "destination_id" : uidTo,
    "type" : type
  }

  let promise = new Promise((resolve, reject) => {
    db.collection('USER').doc(uidTo).get().then(resp => {

      console.log('call db done')

      console.log('resp', resp)

      let resultuser : any = {uid : resp.id ,... resp.data()}

      resultData.destination = resultuser.name

      resolve(resultData)

    }).catch(error => {
      reject(error)
    })
  })

  return promise

}

export function decryptQRData(data: any){

  let qrData = data.qrData

  let qrDecryptData = decryptData(qrData)

  let qrDecryptDataArray = qrDecryptData.split('#')
  let uidTo = qrDecryptDataArray[0]
  let type = qrDecryptDataArray[1]
  let qrValue = qrDecryptDataArray[2]

  let resultData : any = {
    "destination_id" : uidTo,
    "value" : qrValue,
    "type" : type
  }

  let promise = new Promise((resolve, reject) => {

    if (type == "transfer_in" || type == "transfer_out"){

      db.collection('USER').doc(uidTo).get().then(resp => {

        console.log('call db done')
  
        console.log('resp', resp)
  
        let resultuser : any = {uid : resp.id ,... resp.data()}
  
        resultData.destination = resultuser.name
  
        resolve(resultData)
      }).catch(error => {
        reject(error)
      })

    }else{ // Earn or Spend

      db.collection('SHOP').doc(uidTo).get().then(resp => {

        console.log('call db done')
  
        console.log('resp', resp)
  
        var shop : any = {uid : resp.id ,... resp.data()}

        db.collection('SHOP').doc(uidTo).collection('ITEM').get().then(itemResp => {
          let items : any = []
          itemResp.forEach(element => {
            items.push({uid : element.id , ... element.data() })
          });

          shop.item = items

          resultData.destination = shop

          resolve(resultData)

        }).catch(error => {
          reject(error)
        })
      }).catch(error => {
        reject(error)
      })
    }

    

  })

  return promise
}

export function exchangeCoin(data: any) {

  let qrType = data.qrType
  let uidFrom = data.from
  let uidTo = data.to
  let qrValue = data.value
  var item = null

  if (qrType == "earn" || qrType == "spend"){
    item = data.item
  }

  console.log('uidTo', uidTo)
  console.log('qrValue', qrValue)

  const transData = {
    uid_from: uidFrom,
    uid_to: uidTo,
    qr_type: qrType,
    item : item,
    value : Number.parseFloat(qrValue),
    created: new Date()
  }

  let promise = new Promise((resolve, reject) => {

    if (qrType == "spend"){
      getTransactionHistoryAndBalance(uidFrom).then((resp : any) => {
        let balance = calculateBalance(resp , uidFrom)
        if (balance < qrValue){
          reject("AriCoin not enough.")
        }else{
          db.collection('TRANSACTION').add(transData).then(resp => {

            console.log('call db done')
      
            console.log('resp', resp)
      
            resolve(resp)
          }).catch(error => {
            reject(error)
          })
        }
      }).catch(error => {
        reject(error)
      })
    }else{
      db.collection('TRANSACTION').add(transData).then(resp => {

        console.log('call db done')
  
        console.log('resp', resp)
  
        resolve(resp)
      }).catch(error => {
        reject(error)
      })
    }
  })

  return promise

}

export function getUser(uid : any) {

  let userData : any = {}

  let promise = new Promise((resolve, reject) => {

    db.collection('USER').doc(uid).get().then(userResp => {

      console.log(userResp.exists)

      if (userResp.exists){

        userData = { uid : userResp.id, ...userResp.data() , transaction : []}

        getTransactionHistoryAndBalance(uid).then((resp : any) => {
          userData.transaction = resp
          userData.balance = calculateBalance(resp , uid)
          resolve(userData)
        })
      }else{
        reject("No user")
      }
    })
    .catch(error => {
      reject(error)
    })

  })

  return promise

}

export function checkExistUser(uid : any) {
  let userData : any = {}

  console.log('checkExistUser' , uid)

  let promise = new Promise((resolve, reject) => {

    db.collection('USER').doc(uid).get().then(userResp => {

      console.log(userResp.exists)

      if (userResp.exists){

        userData = { uid : userResp.id, ...userResp.data()}

        resolve(userData)
      }else{
        reject("No user")
      }
    })
    .catch(error => {
      reject(error)
    })
  })

  return promise
}

export function createUser(data : any) {
  
  console.log('createUser' , data)

  let newData : any = {
    username : data.username,
    mobileNo : data.mobileNo,
    relation : data.relation,
    activity : data.activity,
    thing : data.thing,
    isFullProfile : false,
    created : new Date(),
    referralCode : randomUid(8),
    referralUsed : 0
  }

  console.log('user' , newData)

  let promise = new Promise((resolve, reject) => {

    db.collection('USER').doc(data.uid).set(newData).then(fromResp => {
      resolve(fromResp)
    }).catch(error => {
      reject("Error create user : " + error)
    })
  })

  return promise

}

export function reward(data : any) {
  let uid = data.uid
  let rewardItem = data.item

  console.log('reward')

  let promise = new Promise((resolve, reject) => {

    db.collection('SHOP').where('name' , '==' , 'Reward').get().then(shopResp => {
      
      let shop : any = {}
      let item : any = {}
      
      shopResp.forEach(element => {
        shop = { uid : element.id, ...element.data()}
      });

      console.log('shop' , shop)

      db.collection('SHOP').doc(shop.uid).collection('ITEM').where('name_en' , '==' , rewardItem).get().then(itemResp => {

        itemResp.forEach(element => {
          item = { uid : element.id, ...element.data()}
        });

        console.log('item' , item)

        const transData = {
          uid_from: uid,
          uid_to: shop.uid,
          qr_type: "earn",
          item : item.uid,
          value : item.value ,
          created: new Date()
        }

        console.log('reward item' , transData)
    
        db.collection('TRANSACTION').add(transData).then(resp => {
    
          console.log('call db done')
    
          console.log('resp', resp)
    
          resolve(resp)
        }).catch(error => {
          reject(error)
        })

      })
      .catch(error => {
        reject(error)
      })
      

    })
    .catch(error => {
      reject(error)
    })
  
    

  })

  return promise
}

export function updateUser(data: any){

  console.log(data)

  var updateFullProfile = false

  var newData : any = {
    firstname : data.firstname,
    lastname : data.lastname ,
    birthday : new Date(data.birthday),
    email : data.email,
    gender : data.gender,
    occupation : data.occupation
  }

  if (
    newData.firstname != "" &&
    newData.lastname != "" &&
    newData.email != "" &&
    newData.gender != "" &&
    newData.occupation != ""
  ){
    updateFullProfile = true
  }else{
    updateFullProfile = false
  }

  if (data.isFullProfile == false && updateFullProfile == true){
    newData.isFullProfile = true
  }

  let promise = new Promise((resolve, reject) => {

    if (data.isFullProfile == false && updateFullProfile == true){
        
      let ownerRewand = {
        uid : data.uid ,
        item : "Full Profile"
      }

      console.log("full" , newData)
  
      reward(ownerRewand).then(resp => {
        db.collection('USER').doc(data.uid).update(newData).then(resp => {
          resolve(true)
        }).catch(error => {
          reject(error)
        })
      }).catch(error => {
        db.collection('USER').doc(data.uid).update(newData).then(resp => {
          resolve(true)
        }).catch(error => {
          reject(error)
        })
      })
    }else{

      console.log(newData)

      db.collection('USER').doc(data.uid).update(newData).then(resp => {
        resolve(true)
      }).catch(error => {
        reject(error)
      })
    }
  })

  return promise
}

export function checkReferral(data : any){
  let referralCode =  data.referralCode

  let promise = new Promise((resolve, reject) => {
    
    db.collection('USER').where('referralCode' , '==' , referralCode).get().then(userResp => {

      let userRef : any = {}

      if (userResp.empty){
        console.log('Incorrect Referral code.')
        reject("Incorrect Referral code.")
      }else{

        userResp.forEach(element => {
          userRef = {id : element.id ,... element.data()}
        });

        console.log("userRef" , userRef)
        console.log("CONST_REFERAL" , CONST_REFERAL.LIMIT)
        console.log("userRef.referralUsed" , parseInt(userRef.referralUsed))

        if (userRef.referralUsed < CONST_REFERAL.LIMIT){
          let ownerRewand = {
            uid : userRef.id ,
            item : "Referral"
          }

          reward(ownerRewand).then(resp => {
            db.collection('USER').doc(userRef.id).update({referralUsed : userRef.referralUsed + 1}).then(resp => {
              resolve(true)
            }).catch(error => {
              reject("Error update Referral used.")
            })
          }).catch(error => {
            reject("Error reward Referral.")
          })
        }else{
          reject("Referral over limit.")
        }
      }

    }).catch(error => {
      reject(error)
    })
  })

  return promise

}

export function redeem(data : any){
  let uid = data.uid
  let shopId = data.shopId
  let itemId = data.itemId
  let redeemCode = data.redeemCode

  let promise = new Promise((resolve, reject) => {

    db.collection('REDEEM').doc(redeemCode).get().then(redeemResp => {

      let redeemDoc : any = {id : redeemResp.id ,... redeemResp.data()}

      if(redeemDoc.available){

        db.collection('SHOP').doc(shopId).collection('ITEM').doc(itemId).get().then(itemResp => {

          let item : any = { uid : itemResp.id, ...itemResp.data()}
  
          console.log('item' , item)
  
          const transData = {
            uid_from: uid,
            uid_to: shopId ,
            qr_type: "earn",
            item : item.uid,
            value : item.value ,
            created: new Date()
          }
  
          console.log('reward item' , transData)
      
          db.collection('TRANSACTION').add(transData).then(resp => {
      
            console.log('call db done')

            let updateData = {
              available : false ,
              used_date : new Date()
            }

            db.collection('REDEEM').doc(redeemCode).update(updateData).then(updateResp => {
              resolve(updateResp)
            }).catch(error => {
              reject(error)
            })
      
          }).catch(error => {
            reject(error)
          })
  
        })
        .catch(error => {
          reject(error)
        })
      }else{
        reject("redeem not available")
      }
    }).catch(error => {
      reject(error)
    })
  })

  return promise
}

export function getDonateLocation(){

  let donateList : any = []

  let promise = new Promise((resolve, reject) => {

    db.collection('DONATE').get().then(resp => {

      donateList = []

      resp.forEach(element => {
        let donateObj = {uid : element.id ,... element.data()}
        donateList.push(donateObj)
      });

      resolve(donateList)
    }).catch(error => {
      reject(error)
    })

  })

  return promise
}

export function createTicket(data : any){
  
  let uid = data.uid
  let shopId = data.shopId
  let itemId = data.itemId

  let promise = new Promise((resolve, reject) => {
  
    let ticketData = {
      uid : uid,
      shop_id : shopId,
      item_id : itemId,
      created : new Date()
    }

    db.collection('EVENT_TICKET').add(ticketData).then(resp => {
      resolve(resp)
    }).catch(error => {
      reject(error)
    })

  })

  return promise
}

export function getTicketsByUser(data : any){

  let uid = data.uid

  console.log("getTicketsByUser")

  let promise = new Promise((resolve, reject) => {

    let tickets : any = []

    db.collection('EVENT_TICKET').where("uid" , "==" , uid).orderBy("created" , "desc").get().then(resp => {

      console.log("get ticket")

      
      resp.forEach(element => {
        let ticket = {id : element.id ,... element.data()}
        tickets.push(ticket)
      });

      console.log("get item in ticket")
        
      let promiseArray : any = []

      tickets.forEach((element : any) => {
        promiseArray.push(new Promise((resolve , reject) => {
          db.collection("SHOP").doc(element.shop_id).collection("ITEM").doc(element.item_id).get().then(itemResp => {
            let item : any = {id : itemResp.id ,... itemResp.data()}
            element.item = item
            resolve(true)
          })
        }))
      });
      

      Promise.all(promiseArray).then(resp => {
        console.log("tickets.length" , tickets.length)
        if(tickets.length > 0){
          resolve(tickets)
        }else{
          reject("No ticket")
        }
        
      }).catch(error => {
        reject(error)
      })
      
    }).catch(error => {
      reject(error)
    })
  })

  return promise
}

// private sub function

function getTransactionHistoryAndBalance(uid : any){

  console.log('getTransactionHistoryAndBalance')

  let promise = new Promise((resolve, reject) => {

    var txList : any = []
    let promiseArray = []

    promiseArray.push(new Promise((resolve , reject) => {

      console.log('getTransactionHistory get From')
      db.collection('TRANSACTION').where('uid_from' , '==' , uid).get().then(fromResp => {

        let resp : any = []

        let destinationPromiseArray : any = []

        fromResp.forEach(element => {

          let tmp : any = {uid : element.id , ... element.data()}

          destinationPromiseArray.push(new Promise((resolve , reject) => {

            if (tmp.qr_type == 'earn' || tmp.qr_type == 'spend'){

              db.collection('SHOP').doc(tmp.uid_to).get().then(shop => {
                let shopdata : any = {uid : shop.id , ... shop.data()}
                tmp.uid_to_name = shopdata.name
                resolve(tmp)
              })
          
            }else{ // UID Transfer To destination

              db.collection('USER').doc(tmp.uid_from).get().then(userFrom => {
                let userdataFrom : any = {uid : userFrom.id , ... userFrom.data()}
                tmp.uid_from_name = userdataFrom.firstname + " " + userdataFrom.lastname

                db.collection('USER').doc(tmp.uid_to).get().then(userTo => {
                  let userdataTo : any = {uid : userTo.id , ... userTo.data()}
                  tmp.uid_to_name = userdataTo.firstname + " " + userdataTo.lastname
                  resolve(tmp)
                })
              })

            }


          }))
        })

        

        Promise.all(destinationPromiseArray).then(response => { 
          response.forEach(element => {
            resp.push(element)
          });
          resolve(resp)
        })
        .catch(promiseErr => {
          reject(promiseErr)
        })

      }).catch(fromErr => {
        reject(fromErr)
      })
    })) 

    // Source transfer to UID
    console.log('getTransactionHistory get to')
    promiseArray.push(new Promise((resolve , reject) => {
      db.collection('TRANSACTION').where('uid_to' , '==' , uid).get().then(fromResp => {

        let resp : any = []

        fromResp.forEach(element => {
          let tmp : any = {uid : element.id , ... element.data()}

          resp.push(tmp)
        });

        resolve(resp)

      }).catch(fromErr => {
        reject(fromErr)
      })
    })) 

    Promise.all(promiseArray).then(promiseResp => {
      console.log('getTransactionHistory promise' , promiseResp) 

      let fromResp : any = promiseResp[0]
      console.log('getTransactionHistory fromResp' , fromResp) 
      fromResp.forEach((element : any) => {
        txList.push(element)
      });

      let toResp : any = promiseResp[1]
      console.log('getTransactionHistory toResp' , toResp) 
      toResp.forEach((element : any) => {
        txList.push(element)
      });

      console.log('getTransactionHistory result' , txList)

      txList = sortingTransactionByDateDESC(txList)

      return resolve(txList)
    })

  })

  return promise
}

function sortingTransactionByDateDESC(txList : any){



  let newList = txList.sort((n1: { created: any } , n2: { created: any }) => {

    let time1 = n1.created._seconds
    let time2 = n2.created._seconds

    if (time1 < time2) {
        return 1;
    }

    if (time1 > time2) {
        return -1;
    }

    return 0;
  });

  return newList

}

function calculateBalance(txList : any , uid : any){

  let balance = 0

  txList.forEach((element : any) => {

    if (element.qr_type == 'earn'){
      balance += element.value
    }else if (element.qr_type == 'spend'){
      balance -= element.value
    }else{
      if (element.uid_from == uid){
        balance -= element.value
      }else {
        balance += element.value
      }
    }
  });

  return balance
}