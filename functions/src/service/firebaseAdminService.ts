import * as admin from 'firebase-admin';
import { randomUid } from './constant';

const db = admin.firestore();

export function moveAllDocument(data: any) {

  const source = data.source
  const target = data.target

  let promise = new Promise((resolve, reject) => {

    new Promise((resolve, reject) => {
      let sourceResult : any = []
      db.collection(source).get().then(resp => {
        resp.forEach(element => {
          sourceResult.push(element.data())
        });
        resolve(sourceResult)
      })
    }).then((sourceResp : any) => {

      let promiseArray : any = []

      
      sourceResp.forEach((element : any) => {
      
        promiseArray.push(new Promise((resolve, reject) => {
          db.collection(target).add(element).then(resp => {
            resolve(true)
          }).catch(error => {
            reject(error)
          })
        }))
      })

      Promise.all(promiseArray).then(resp => {
        resolve(true)
      }).catch(error => {
        reject(error)
      })
    
    }).catch(error => {
      reject(error)
    })

  })

  return promise

}

export function addShop(data: any) {

  let promise = new Promise((resolve, reject) => {
    let newData : any = data
    newData.created = new Date()

    db.collection("SHOP").add(newData).then(resp => {
      resolve(resp.id)
    }).catch(err => {
      reject(err)
    })
  })

  return promise
}

export function addShopItem(data: any) {

  let shopId = data.shopId
  let item = data.item

  let promise = new Promise((resolve, reject) => {
    let itemData : any = item
    itemData.created = new Date()

    db.collection("SHOP").doc(shopId).collection("ITEM").add(itemData).then(resp => {
      resolve(resp.id)
    }).catch(err => {
      reject(err)
    })
  })

  return promise
}

export function generateRedeemCode(data : any){
  let shopId = data.shopId
  let itemId = data.itemId
  let amount = data.amount

  var promiseArray : any = []
  var redeemCodeArray : any = []

  let promise = new Promise((resolve, reject) => {

    promiseArray = []

    for (var i=0 ; i<amount ; i++){
    
      promiseArray.push(new Promise((resolve, reject) => {
  
        let code = randomUid(6)
  
        let redeemData = {
          shop_id : shopId,
          item_id : itemId,
          available : true,
          created_date : new Date()
        }
  
        db.collection('REDEEM').doc(code).set(redeemData).then(resp => {
          redeemCodeArray.push(code)
          resolve(true)
        }).catch(error => {
          reject(error)
        })
      }))
    }

    Promise.all(promiseArray).then(resp => {
      resolve(redeemCodeArray)
    })
  })

  
  return promise
  
}


export function addDonateLocation(data : any){

  let name = data.name
  let description = data.description
  
  let require : any = data.require
  let contact : any = data.contact
  let condition = data.condition

  let promise = new Promise((resolve, reject) => {

    var newData : any = {
      name : name ,
      description : description,
      condition : condition,
      require : require,
      contact : contact,
      create : new Date()
    }

    if (data.location != null){
      let location : any = data.location

      newData.location = {
        lat : parseFloat(location.lat),
        lon : parseFloat(location.lon)
      }
      
    }

    db.collection('DONATE').add(newData).then(resp => {
      resolve(resp)
    }).catch(error => {
      reject(error)
    })
    
  })

  
  return promise
  
}