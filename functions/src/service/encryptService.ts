const crypto = require('crypto');
const algorithm = 'aes-128-cbc';
const ENCRYPTION_KEY = '-ARIAround-2021-'
const IV_LENGTH = 16;

// Pattern
// target uid#type

export function encryptData(data : any){

  let iv = crypto.randomBytes(IV_LENGTH);
  let key = Buffer.from(ENCRYPTION_KEY).toString('base64')
  console.log('key' , key.substring(0,16))

  let cipher = crypto.createCipheriv(algorithm, key.substring(0,16), iv)
  let encrypted = cipher.update(data)
  encrypted = Buffer.concat([encrypted, cipher.final()])

  console.log('encrypt data' , encrypted.toString('hex'))

  return iv.toString('hex') + ':' + encrypted.toString('hex')

}

export function decryptData(data : any){

  let key = Buffer.from(ENCRYPTION_KEY).toString('base64')
  console.log('key' , key.substring(0,16))

  let textParts = data.split(':');
  let iv = Buffer.from(textParts.shift(), 'hex');
  let encryptedText = Buffer.from(textParts.join(':'), 'hex');

  console.log('encryptText' , encryptedText)
  let decipher = crypto.createDecipheriv(algorithm, key.substring(0,16), iv)
  let decrypted = decipher.update(encryptedText)

  console.log('decrypted' , decrypted)
  decrypted = Buffer.concat([decrypted, decipher.final()])

  console.log('decrypted data' , decrypted.toString())

  return decrypted.toString()

}