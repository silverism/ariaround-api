import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';

import { firebaseConfig } from "./config/firebase.config";
admin.initializeApp(firebaseConfig);

import { 
  decryptQRData, 
  getShopList, 
  getUser, 
  exchangeCoin , 
  updateUser , 
  scanQRForSpend , 
  createUser , 
  reward , 
  checkExistUser ,
  checkReferral ,
  scanQRForEarn,
  getShopItem,
  redeem,
  getDonateLocation,
  createTicket,
  getTicketsByUser
} from "./service/firebaseService"

import { getConstant } from "./service/firebaseConstantService"


const firebaseHTTPInterfaceApp = require('./interface/firebaseHTTPInterface')
exports.FirebaseHTTPService = functions.https.onRequest(firebaseHTTPInterfaceApp);

exports.getShopList = functions.https.onCall((data, context) => {

  console.log('getShopList')
  
  return getShopList(data).then(promiseResp => {
    console.log('return resolve', promiseResp)
    return { date: new Date(), status: "Complete", data: promiseResp }
  }).catch(error => {
    console.log('return error', error)
    return { date: new Date(), status: "Error", data: error }
  })

});

exports.getShopItem = functions.https.onCall((data, context) => {

  console.log('getShopItem')
  
  return getShopItem(data).then(promiseResp => {
    console.log('return resolve', promiseResp)
    return { date: new Date(), status: "Complete", data: promiseResp }
  }).catch(error => {
    console.log('return error', error)
    return { date: new Date(), status: "Error", data: error }
  })

});

exports.getUser = functions.https.onCall((data, context) => {
  
  return getUser(data).then(promiseResp => {
    console.log('return resolve', promiseResp)
    return { date: new Date(), status: "Complete", data: promiseResp }
  }).catch(error => {
    console.log('return error', error)
    return { date: new Date(), status: "Error", data: error }
  })

});

exports.checkExistUser = functions.https.onCall((data, context) => {
  
  return checkExistUser(data).then(promiseResp => {
    console.log('return resolve', promiseResp)
    return { date: new Date(), status: "Complete", data: promiseResp }
  }).catch(error => {
    console.log('return error', error)
    return { date: new Date(), status: "Error", data: error }
  })

});


exports.createUser = functions.https.onCall((data, context) => {
  
  return createUser(data).then(promiseResp => {
    console.log('return resolve', promiseResp)
    return { date: new Date(), status: "Complete", data: promiseResp }
  }).catch(error => {
    console.log('return error', error)
    return { date: new Date(), status: "Error", data: error }
  })

});

exports.reward = functions.https.onCall((data, context) => {
  
  return reward(data).then(promiseResp => {
    console.log('return resolve', promiseResp)
    return { date: new Date(), status: "Complete", data: promiseResp }
  }).catch(error => {
    console.log('return error', error)
    return { date: new Date(), status: "Error", data: error }
  })

});

exports.decryptQRData = functions.https.onCall((data, context) => {
  return decryptQRData(data).then(promiseResp => {
    console.log('return resolve', promiseResp)
    return { date: new Date(), status: "Complete", data: promiseResp }
  }).catch(error => {
    console.log('return error', error)
    return { date: new Date(), status: "Error", data: error }
  }) 
});

exports.exchangeCoin = functions.https.onCall((data, context) => {
  return exchangeCoin(data).then(promiseResp => {
    console.log('return resolve', promiseResp)
    return { date: new Date(), status: "Complete", data: promiseResp }
  }).catch(error => {
    console.log('return error', error)
    return { date: new Date(), status: "Error", data: error }
  })

});

exports.constant = functions.https.onCall((data, context) => {
  return getConstant().then(promiseResp => {
    console.log('return resolve', promiseResp)
    return { date: new Date(), status: "Complete", data: promiseResp }
  }).catch(error => {
    console.log('return error', error)
    return { date: new Date(), status: "Error", data: error }
  })

});

exports.updateUser = functions.https.onCall((data, context) => {
  return updateUser(data).then(promiseResp => {
    console.log('return resolve', promiseResp)
    return { date: new Date(), status: "Complete", data: promiseResp }
  }).catch(error => {
    console.log('return error', error)
    return { date: new Date(), status: "Error", data: error }
  }) 
})

exports.scanQRForSpend = functions.https.onCall((data, context) => {
  return scanQRForSpend(data).then(promiseResp => {
    console.log('return resolve', promiseResp)
    return { date: new Date(), status: "Complete", data: promiseResp }
  }).catch(error => {
    console.log('return error', error)
    return { date: new Date(), status: "Error", data: error }
  }) 
});

exports.scanQRForEarn = functions.https.onCall((data, context) => {
  return scanQRForEarn(data).then(promiseResp => {
    console.log('return resolve', promiseResp)
    return { date: new Date(), status: "Complete", data: promiseResp }
  }).catch(error => {
    console.log('return error', error)
    return { date: new Date(), status: "Error", data: error }
  }) 
});

exports.checkReferral = functions.https.onCall((data, context) => {
  
  return checkReferral(data).then(promiseResp => {
    console.log('return resolve', promiseResp)
    return { date: new Date(), status: "Complete", data: promiseResp }
  }).catch(error => {
    console.log('return error', error)
    return { date: new Date(), status: "Error", data: error }
  })

});

exports.redeem = functions.https.onCall((data, context) => {
  
  return redeem(data).then(promiseResp => {
    console.log('return resolve', promiseResp)
    return { date: new Date(), status: "Complete", data: promiseResp }
  }).catch(error => {
    console.log('return error', error)
    return { date: new Date(), status: "Error", data: error }
  })

});

exports.getDonateLocation = functions.https.onCall((data, context) => {
  
  return getDonateLocation().then(promiseResp => {
    console.log('return resolve', promiseResp)
    return { date: new Date(), status: "Complete", data: promiseResp }
  }).catch(error => {
    console.log('return error', error)
    return { date: new Date(), status: "Error", data: error }
  })

});

exports.createTicket = functions.https.onCall((data, context) => {
  
  return createTicket(data).then(promiseResp => {
    console.log('return resolve', promiseResp)
    return { date: new Date(), status: "Complete", data: promiseResp }
  }).catch(error => {
    console.log('return error', error)
    return { date: new Date(), status: "Error", data: error }
  })

});

exports.getTicketsByUser = functions.https.onCall((data, context) => {
  
  return getTicketsByUser(data).then(promiseResp => {
    console.log('return resolve', promiseResp)
    return { date: new Date(), status: "Complete", data: promiseResp }
  }).catch(error => {
    console.log('return error', error)
    return { date: new Date(), status: "Error", data: error }
  })

});

// exports.triggerUpdateBalance = functions.firestore.document('TRANSACTION/{txId}').onCreate((snap, context) => {

//   const txData = snap.data();

//   let promiseArray = []

//   const uidFrom = txData.uid_from
//   const uidTo = txData.uid_to
//   const value = Number.parseFloat(txData.value)
//   const qrType = txData.type

//   if (qrType == "earn" || qrType == "spend"){
//     promiseArray.push(new Promise((resolve , reject) => {
      
//       var increment : any

//       if (qrType == "earn"){ // uidFrom +
//         increment = admin.firestore.FieldValue.increment(value);
//       }else{
//         increment = admin.firestore.FieldValue.increment(value * (-1));
//       }

//       db.collection('USER').doc(uidFrom).update({
//         balance : increment,
//         last_upd : new Date()
//       }).then(resp => {
//         console.log("Complete : Update user data - Update balance")
//         resolve(true)
//       }).catch(error => {
//         console.log("ERROR : Update user data - Update balance" , error)
//         reject(error)
//       })
//     }))
//   }else{

//     promiseArray.push(new Promise((resolve , reject) => {
      
//       var increment = admin.firestore.FieldValue.increment(value);

//       db.collection('USER').doc(uidTo).update({
//         balance : increment,
//         last_upd : new Date()
//       }).then(resp => {
//         console.log("update history uidFrom done")
//         resolve(true)
//       }).catch(error => {
//         reject(error)
//       })
//     }))

//     promiseArray.push(new Promise((resolve , reject) => {
      
//       var increment = admin.firestore.FieldValue.increment(value * (-1));

//       db.collection('USER').doc(uidFrom).update({
//         balance : increment,
//         last_upd : new Date()
//       }).then(resp => {
//         console.log("update history uidFrom done")
//         resolve(true)
//       }).catch(error => {
//         reject(error)
//       })
//     }))

//   }

//   Promise.all(promiseArray).then(resp => {
//     console.log("update history done")
//   }).catch(err => {
//     console.log("update history error" , err)
//   })

// })